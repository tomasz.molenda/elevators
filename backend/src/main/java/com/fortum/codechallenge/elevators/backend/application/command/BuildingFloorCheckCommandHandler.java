package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.fortum.codechallenge.elevators.backend.application.eventbus.WaitingBuildingFloorFoundedEvent;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloorRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class BuildingFloorCheckCommandHandler implements CommandHandler<BuildingFloorCheckCommand> {

    BuildingFloorRepository buildingFloorRepository;
    EventSender eventSender;

    @Override
    public void handle(BuildingFloorCheckCommand command) {

        buildingFloorRepository.listAll()
                .stream()
                .filter(buildingFloor -> isNotEmpty(buildingFloor.getWaitingDirections()))
                .forEachOrdered(this::sendEvent);
    }

    private void sendEvent(BuildingFloor buildingFloor) {
        buildingFloor.getWaitingDirections()
                .forEach(direction -> eventSender.send(new WaitingBuildingFloorFoundedEvent(buildingFloor.getFloor(), direction)));
    }
}
