package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.fortum.codechallenge.elevators.backend.application.eventbus.ElevatorRequestedEvent;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.ElevatorRepository;
import com.fortum.codechallenge.elevators.backend.domain.Floor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import static com.fortum.codechallenge.elevators.backend.common.Direction.NONE;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class ElevatorInternalRequestCommandHandler implements CommandHandler<ElevatorInternalRequestCommand> {

    ElevatorRepository elevatorRepository;
    EventSender eventSender;

    @Override
    public void handle(ElevatorInternalRequestCommand command) {

        Integer id = command.getId();
        Integer floorNumber = command.getFloorNumber();

        Elevator elevator = elevatorRepository.requiredGet(id);

        if (elevator.getCurrentFloor().equals(floorNumber)) {
            return;
        }

        boolean isNotBusy = !elevator.isBusy();

        elevator.addFloor(new Floor(floorNumber, NONE));

        if (isNotBusy) {
            eventSender.send(new ElevatorRequestedEvent(elevator.getId()));
        }
    }
}
