package com.fortum.codechallenge.elevators.backend.infrastructure.eventbus;

import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.google.common.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class EventBusAutoConfigurationForTest {

    @Bean
    public EventBus eventBus() {
        return new EventBus();
    }

    @Bean
    public EventSender eventBusSender() {
        return new EventBusSender(eventBus());
    }

    @Bean
    public EventBusSubscriberBeanPostProcessor subscriberAnnotationProcessor() {
        return new EventBusSubscriberBeanPostProcessor(eventBus());
    }
}
