package com.fortum.codechallenge.elevators.backend.common;

/**
 * Enumeration for describing elevator's direction.
 */
public enum Direction {
    UP, DOWN, NONE;

    public boolean isUp() {
        return this.equals(UP);
    }

    public boolean isDown() {
        return this.equals(DOWN);
    }

    public boolean isNone() {
        return this.equals(NONE);
    }

    public boolean isNotNone() {
        return !this.equals(NONE);
    }

}
