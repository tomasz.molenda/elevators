package com.fortum.codechallenge.elevators.backend.intefaces.init;

import com.fortum.codechallenge.elevators.backend.application.Gate;
import com.fortum.codechallenge.elevators.backend.application.command.BuildingFloorCreateCommand;
import com.fortum.codechallenge.elevators.backend.common.ConfigurationProvider;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class FloorsInitiator implements ApplicationListener<ApplicationReadyEvent> {

    ConfigurationProvider configurationProvider;
    Gate gate;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        int minimumNumberOfFloor = configurationProvider.getMinimumNumberOfFloor();
        int maximumNumberOfFloor = configurationProvider.getMaximumNumberOfFloor();

        IntStream.range(minimumNumberOfFloor, maximumNumberOfFloor + 1)
                .forEach(this::createFloor);

    }

    private void createFloor(Integer number) {
        gate.dispatch(new BuildingFloorCreateCommand(number));
    }
}
