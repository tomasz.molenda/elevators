package com.fortum.codechallenge.elevators.backend.infrastructure.eventbus;

import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.Executors;

@Configuration
@Profile("!test")
public class EventBusAutoConfiguration {

    @Value("${com.fortum.codechallenge.numberOfElevators}")
    private int numberOfElevators;

    @Bean
    public EventBus eventBus() {
        return new AsyncEventBus(Executors.newScheduledThreadPool(numberOfElevators));
    }

    @Bean
    public EventSender eventBusSender() {
        return new EventBusSender(eventBus());
    }

    @Bean
    public EventBusSubscriberBeanPostProcessor subscriberAnnotationProcessor() {
        return new EventBusSubscriberBeanPostProcessor(eventBus());
    }
}
