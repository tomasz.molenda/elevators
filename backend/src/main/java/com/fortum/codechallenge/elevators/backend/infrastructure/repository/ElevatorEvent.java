package com.fortum.codechallenge.elevators.backend.infrastructure.repository;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Getter
public class ElevatorEvent {

    Integer elevatorId;
    Long version;
    Object event;
    String name;
}
