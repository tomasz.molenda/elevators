package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloorRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class BuildingFloorCreateCommandHandler implements CommandHandler<BuildingFloorCreateCommand> {

    BuildingFloorRepository buildingFloorRepository;

    @Override
    public void handle(BuildingFloorCreateCommand command) {

        Integer number = command.getNumber();

        BuildingFloor floor = new BuildingFloor(number);
        buildingFloorRepository.save(floor);
    }
}
