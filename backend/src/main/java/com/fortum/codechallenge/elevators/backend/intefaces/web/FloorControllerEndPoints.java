package com.fortum.codechallenge.elevators.backend.intefaces.web;

import com.fortum.codechallenge.elevators.backend.application.query.BuildingFloorFinder;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/v1/floors")
@CrossOrigin(origins = "http://localhost:3002")
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class FloorControllerEndPoints {

    BuildingFloorFinder buildingFloorFinder;

    @GetMapping
    List<BuildingFloor> listBuildingFloors() {
        return buildingFloorFinder.listBuildingFloor();
    }
}
