package com.fortum.codechallenge.elevators.backend.common;

public interface Event {

    default Integer getId() {
        return 0;
    }
}
