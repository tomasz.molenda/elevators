
package com.fortum.codechallenge.elevators.backend.common.utils;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class StreamUtils {

    public static <E> Optional<E> findOneElement(List<E> list, Predicate<E> predicate) {

        return list.stream()
                .filter(predicate)
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple elements");
                });
    }

}
