package com.fortum.codechallenge.elevators.backend.application.service.nearestElevator;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;

interface NearestElevatorStrategy {

    boolean isApplicable(Elevator elevator, Direction direction, Integer floorNumber);
    Elevator apply(Elevator elevator);
}
