package com.fortum.codechallenge.elevators.backend.application.service.nearestElevator;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import org.springframework.stereotype.Service;

@Service
class NotBusyStrategy implements NearestElevatorStrategy {

    @Override
    public boolean isApplicable(Elevator elevator, Direction direction, Integer floorNumber) {
        return isNotBusy(elevator);
    }

    @Override
    public Elevator apply(Elevator elevator) {
        return elevator;
    }

    private boolean isNotBusy(Elevator elevator) {
        return !elevator.isBusy();
    }
}
