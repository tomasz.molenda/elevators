package com.fortum.codechallenge.elevators.backend.infrastructure.cqrs;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class RunEnvironment {

    HandlersProvider handlersProvider;

    void run(Object command) {

        CommandHandler<Object> handler = handlersProvider.getHandler(command);

        handler.handle(command);
    }
}
