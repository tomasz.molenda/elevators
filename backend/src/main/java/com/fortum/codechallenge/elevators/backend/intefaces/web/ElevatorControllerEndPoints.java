package com.fortum.codechallenge.elevators.backend.intefaces.web;

import com.fortum.codechallenge.elevators.backend.application.Gate;
import com.fortum.codechallenge.elevators.backend.application.command.ElevatorInternalRequestCommand;
import com.fortum.codechallenge.elevators.backend.application.command.FloorRequestElevatorCommand;
import com.fortum.codechallenge.elevators.backend.application.query.ElevatorFinder;
import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest Resource.
 */
@RestController
@RequestMapping("/rest/v1/elevators")
@CrossOrigin(origins = "http://localhost:3002")
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class ElevatorControllerEndPoints {

    Gate gate;
    ElevatorFinder elevatorFinder;

    @PostMapping(value = "/request/{floorNumber}/{direction}")
    void requestElevator(@PathVariable("floorNumber") Integer floorNumber, @PathVariable("direction") Direction direction) {
        gate.dispatch(new FloorRequestElevatorCommand(floorNumber, direction));
    }

    @PostMapping(value = "{id}/move/floor/{floorNumber}")
    void internalRequestElevator(@PathVariable("id") Integer id, @PathVariable("floorNumber") Integer floorNumber) {
        gate.dispatch(new ElevatorInternalRequestCommand(id, floorNumber));
    }

    @GetMapping
    List<Elevator> listElevators() {
        return elevatorFinder.listElevators();
    }
}
