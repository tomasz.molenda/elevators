package com.fortum.codechallenge.elevators.backend.domain;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Getter
public class Floor implements Serializable {

    private static final long serialVersionUID = 1;

    final Integer floor;
    @Setter
    Direction direction;

    public Floor(Integer floor, Direction direction) {
        this.floor = floor;
        this.direction = direction;
    }
}
