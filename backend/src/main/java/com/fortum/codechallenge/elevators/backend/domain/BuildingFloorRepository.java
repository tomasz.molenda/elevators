package com.fortum.codechallenge.elevators.backend.domain;

import java.util.List;

public interface BuildingFloorRepository {

    void save(BuildingFloor floor);

    BuildingFloor requireGet(Integer number);

    List<BuildingFloor> listAll();
}
