package com.fortum.codechallenge.elevators.backend.domain;

import java.util.List;

public interface ElevatorRepository {

    void save(Elevator elevator);

    Elevator requiredGet(Integer id);

    List<Elevator> listAll();
}
