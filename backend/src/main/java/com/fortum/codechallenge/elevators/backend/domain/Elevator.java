package com.fortum.codechallenge.elevators.backend.domain;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.fortum.codechallenge.elevators.backend.common.Direction.*;
import static com.fortum.codechallenge.elevators.backend.common.utils.AssertUtils.isTrue;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.ObjectUtils.notEqual;
import static org.springframework.util.Assert.notNull;

public class Elevator implements Serializable {

    private static final long serialVersionUID = 1;
    @Getter
    private final Integer id;
    private final HandlingTime handlingTime;
    private final List<Floor> floors = Lists.newArrayList();
    @Getter
    private final List<Integer> history = Lists.newArrayList();

    @Getter
    private Integer currentFloor;
    @Getter
    private Direction direction;
    @Getter
    private boolean waiting;

    public Elevator(Integer id, Integer currentFloor, HandlingTime handlingTime) {
        this.id = id;
        this.currentFloor = currentFloor;
        this.handlingTime = handlingTime;
        this.direction = NONE;
        this.waiting = true;
    }

    public boolean hasFloor(Floor floor) {
        notNull(floor, "floor must not be null");
        return floors.contains(floor);
    }

    public boolean isBusy() {
        return isNotEmpty(floors);
    }

    public void addFloor(Floor floor) {
        notNull(floor, "floor must not be null");
        isTrue(floors.stream().map(Floor::getDirection).noneMatch(direction -> isIllegalDirection(direction, floor.getDirection())), ElevatorException::new);
        floors.add(floor);
    }

    private boolean isIllegalDirection(Direction currentDirection, Direction directionToAdd) {
        if (directionToAdd.isNone()) {
            return false;
        }
        if (directionToAdd.equals(direction)) {
            return false;
        }
        return notEqual(currentDirection, directionToAdd);
    }

    @SneakyThrows
    public void move(Consumer<Floor> arrivedConsumer) {
        history.add(currentFloor);

        if (floors.isEmpty()) {
            direction = NONE;
            return;
        }

        if (direction.isDown() && someLowerFloorIsWaiting()) {
            moveDown();
        }
        else if (direction.isUp() && someUpperFloorIsWaiting()) {
            moveUp();
        }
        else if (allWaitingFloorsAreUpper()) {
            moveUp();
        }
        else if (allWaitingFloorsAreLower()) {
            moveDown();
        }
        else if (firstWaitingFloorIsUpper()) {
            moveUp();
        }
        else if (firstWaitingFloorIsLower()) {
            moveDown();
        }

        TimeUnit.MILLISECONDS.sleep(handlingTime.getMovingTime());

        markArrivedToFloor(arrivedConsumer);
        move(arrivedConsumer);
    }

    private void markArrivedToFloor(Consumer<Floor> arrivedConsumer) {
        listCurrentFloorsToMark().forEach(floor -> markAsArrived(floor, arrivedConsumer));
    }

    private List<Floor> listCurrentFloorsToMark() {
        return floors.stream()
                    .filter(floor -> floor.getFloor().equals(currentFloor))
                    .collect(Collectors.toUnmodifiableList());
    }

    @SneakyThrows
    private void markAsArrived(Floor floor, Consumer<Floor> arrivedConsumer) {
        floors.remove(floor);
        arrivedConsumer.accept(floor);
        TimeUnit.MILLISECONDS.sleep(handlingTime.getWaitingTime());
        if (floor.getDirection().isNotNone()) {
            this.direction = floor.getDirection();
        }
    }

    private void moveUp() {
        currentFloor++;
        direction = UP;
    }

    private void moveDown() {
        currentFloor--;
        direction = DOWN;
    }

    private boolean allWaitingFloorsAreLower() {
        return floors.stream()
                .allMatch(floor -> floor.getFloor() < currentFloor);
    }

    private boolean allWaitingFloorsAreUpper() {
        return floors.stream()
                .allMatch(floor -> floor.getFloor() > currentFloor);
    }

    private boolean someUpperFloorIsWaiting() {
        return floors.stream()
                .anyMatch(floor -> floor.getFloor() > currentFloor);
    }

    private boolean someLowerFloorIsWaiting() {
        return floors.stream()
                .anyMatch(floor -> floor.getFloor() < currentFloor);
    }

    public void clearHistory() {
        this.history.clear();
    }

    private boolean firstWaitingFloorIsUpper() {
            return floors.stream()
                .findFirst()
                .map(Floor::getFloor)
                .filter(number -> number > currentFloor)
                .isPresent();
    }

    private boolean firstWaitingFloorIsLower() {
        return floors.stream()
                .findFirst()
                .map(Floor::getFloor)
                .filter(number -> number < currentFloor)
                .isPresent();
    }
}
