package com.fortum.codechallenge.elevators.backend.infrastructure.repository;

import com.fortum.codechallenge.elevators.backend.application.query.ElevatorFinder;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.ElevatorRepository;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
class InMemoryElevatorRepository implements ElevatorRepository, ElevatorFinder {

    private final ConcurrentMap<Integer, Elevator> elevators = new ConcurrentHashMap<>();

    @Override
    public void save(Elevator elevator) {
        elevators.put(elevator.getId(), elevator);
    }

    @Override
    public Elevator requiredGet(Integer id) {
        return elevators.get(id);
    }

    @Override
    public List<Elevator> listAll() {
        return elevators.values().stream()
                .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public List<Elevator> listElevators() {
        return elevators.values().stream()
                .map(SerializationUtils::clone)
                .collect(Collectors.toUnmodifiableList());
    }
}
