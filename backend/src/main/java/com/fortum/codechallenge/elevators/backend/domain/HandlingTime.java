package com.fortum.codechallenge.elevators.backend.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Getter
public class HandlingTime implements Serializable {

    private static final long serialVersionUID = 1;

    int movingTime;
    int waitingTime;
}
