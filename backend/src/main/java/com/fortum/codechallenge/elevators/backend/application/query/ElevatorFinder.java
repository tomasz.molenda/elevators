package com.fortum.codechallenge.elevators.backend.application.query;

import com.fortum.codechallenge.elevators.backend.domain.Elevator;

import java.util.List;

public interface ElevatorFinder {

    List<Elevator> listElevators();
}
