package com.fortum.codechallenge.elevators.backend.application;

public interface Gate {

	void dispatch(Object command);
}
