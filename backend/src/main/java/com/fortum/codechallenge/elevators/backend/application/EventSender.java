package com.fortum.codechallenge.elevators.backend.application;

import com.fortum.codechallenge.elevators.backend.common.Event;

public interface EventSender {

    void send(Event event);
}
