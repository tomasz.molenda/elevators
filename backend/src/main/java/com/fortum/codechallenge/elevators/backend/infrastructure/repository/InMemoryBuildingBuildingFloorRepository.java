package com.fortum.codechallenge.elevators.backend.infrastructure.repository;

import com.fortum.codechallenge.elevators.backend.application.query.BuildingFloorFinder;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloorRepository;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
class InMemoryBuildingBuildingFloorRepository implements BuildingFloorRepository, BuildingFloorFinder {

    private final List<BuildingFloor> buildingFloors = Lists.newArrayList();

    @Override
    public void save(BuildingFloor floor) {
        buildingFloors.add(floor);
    }

    @Override
    public BuildingFloor requireGet(Integer number) {
        return buildingFloors.stream()
                .filter(floor -> floor.getFloor().equals(number))
                .findAny()
                .orElseThrow();
    }

    @Override
    public List<BuildingFloor> listAll() {
        return ImmutableList.copyOf(buildingFloors);
    }

    @Override
    public List<BuildingFloor> listBuildingFloor() {
        return buildingFloors.stream()
                .map(SerializationUtils::clone)
                .collect(Collectors.toUnmodifiableList());
    }
}
