package com.fortum.codechallenge.elevators.backend.intefaces.init;

import com.fortum.codechallenge.elevators.backend.application.Gate;
import com.fortum.codechallenge.elevators.backend.application.command.ElevatorCreateCommand;
import com.fortum.codechallenge.elevators.backend.common.ConfigurationProvider;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ElevatorsInitiator implements ApplicationListener<ApplicationReadyEvent> {

    ConfigurationProvider configurationProvider;
    Gate gate;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        int numberOfElevators = configurationProvider.getNumberOfElevators();

        IntStream.range(1, numberOfElevators + 1)
                .forEach(this::createElevator);

    }

    private void createElevator(Integer id) {
        int startFloor = configurationProvider.getStartFloor();
        ElevatorCreateCommand command = new ElevatorCreateCommand(id, startFloor);
        gate.dispatch(command);
    }
}
