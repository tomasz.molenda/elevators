package com.fortum.codechallenge.elevators.backend.application;

public interface CommandHandler<C> {

    void handle(C command);
}
