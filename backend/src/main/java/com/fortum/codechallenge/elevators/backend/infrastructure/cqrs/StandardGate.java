package com.fortum.codechallenge.elevators.backend.infrastructure.cqrs;

import com.fortum.codechallenge.elevators.backend.application.Gate;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;


@Log4j2
@Component
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class StandardGate implements Gate {

	RunEnvironment runEnvironment;

	@Override
	public void dispatch(Object command) {

		log.info("Received command {}", command.getClass().getSimpleName());

		runEnvironment.run(command);
	}
}
