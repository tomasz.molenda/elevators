package com.fortum.codechallenge.elevators.backend.application.eventbus;

import com.fortum.codechallenge.elevators.backend.common.Event;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Getter
public class ElevatorFinishedEvent implements Event {

    Integer id;
}
