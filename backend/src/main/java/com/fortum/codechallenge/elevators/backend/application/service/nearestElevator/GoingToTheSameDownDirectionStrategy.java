package com.fortum.codechallenge.elevators.backend.application.service.nearestElevator;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import org.springframework.stereotype.Service;

@Service
class GoingToTheSameDownDirectionStrategy implements NearestElevatorStrategy {

    @Override
    public boolean isApplicable(Elevator elevator, Direction direction, Integer floorNumber) {
        return elevator.isBusy()
                && isGoingToTheSameDirection(elevator, direction)
                && direction.isUp()
                && isAtLowerOrTheSameFloor(elevator, floorNumber);
    }

    @Override
    public Elevator apply(Elevator elevator) {
        return elevator;
    }

    private boolean isGoingToTheSameDirection(Elevator elevator, Direction direction) {
        return elevator.getDirection().equals(direction);
    }

    private boolean isAtLowerOrTheSameFloor(Elevator elevator, Integer floorNumber) {
        return elevator.getCurrentFloor() <= floorNumber;
    }
}
