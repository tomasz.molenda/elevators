package com.fortum.codechallenge.elevators.backend.infrastructure.eventbus;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Method;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class EventBusSubscriberBeanPostProcessor implements BeanPostProcessor {

    EventBus eventBus;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {

        Method[] methods = bean.getClass().getMethods();
        for (Method m : methods) {
            if (m.isAnnotationPresent(Subscribe.class)) {
                this.eventBus.register(bean);
                break;
            }
        }
        return bean;
    }
}
