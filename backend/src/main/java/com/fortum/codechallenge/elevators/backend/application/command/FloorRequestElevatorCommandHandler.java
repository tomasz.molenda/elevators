package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.fortum.codechallenge.elevators.backend.application.eventbus.ElevatorRequestedEvent;
import com.fortum.codechallenge.elevators.backend.application.service.nearestElevator.NearestElevatorFindService;
import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.ElevatorRepository;
import com.fortum.codechallenge.elevators.backend.domain.Floor;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloorRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Objects.nonNull;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class FloorRequestElevatorCommandHandler implements CommandHandler<FloorRequestElevatorCommand> {

    EventSender eventSender;
    BuildingFloorRepository buildingFloorRepository;
    NearestElevatorFindService nearestElevatorFindService;
    ElevatorRepository elevatorRepository;

    Lock lock = new ReentrantLock();

    @Override
    public void handle(FloorRequestElevatorCommand command) {

        Integer toFloor = command.getFloor();
        Direction direction = command.getDirection();

        List<Elevator> elevators = elevatorRepository.listAll();

        BuildingFloor buildingFloor = buildingFloorRepository.requireGet(toFloor);

        lock.lock();

        buildingFloor.addDirection(direction);

        Elevator elevator = null;

        try {
            Floor floor = new Floor(toFloor, direction);
            if (floorNotAlreadyRequested(elevators, floor)) {

                Optional<Elevator> nearestElevator = nearestElevatorFindService.findNearestElevator(elevators, buildingFloor.getFloor(), direction);

                if (nearestElevator.isPresent()) {
                    elevator = nearestElevator.get();
                    elevator.addFloor(floor);
                }
            }
        }
        finally {
            lock.unlock();
        }

        if (nonNull(elevator) && isNotStaring(elevator)) {
            eventSender.send(new ElevatorRequestedEvent(elevator.getId()));
        }
    }

    boolean floorNotAlreadyRequested(List<Elevator> elevators, Floor floor) {
        return elevators.stream()
                .noneMatch(elevator -> elevator.hasFloor(floor));
    }

    private boolean isNotStaring(Elevator elevator) {
        return elevator.getDirection().isNone();
    }
}
