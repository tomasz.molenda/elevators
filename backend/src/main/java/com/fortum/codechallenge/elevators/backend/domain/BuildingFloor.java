package com.fortum.codechallenge.elevators.backend.domain;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.google.common.collect.Sets;
import lombok.Getter;

import java.io.Serializable;
import java.util.Set;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

public class BuildingFloor implements Serializable {

    private static final long serialVersionUID = 1;

    @Getter
    final Integer floor;
    @Getter
    Set<Direction> waitingDirections = Sets.newHashSet();

    public BuildingFloor(Integer floor) {
        notNull(floor, "floor must not be null");
        this.floor = floor;
    }

    public void addDirection(Direction direction) {
        notNull(direction, "direction must not be null");
        isTrue(direction.isNotNone(), "direction must not be none");
        waitingDirections.add(direction);
    }

    public void removeDirection(Direction direction) {
        notNull(direction, "direction must not be null");
        waitingDirections.remove(direction);
    }
}
