package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.fortum.codechallenge.elevators.backend.application.eventbus.ElevatorFinishedEvent;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloorRepository;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.ElevatorRepository;
import com.fortum.codechallenge.elevators.backend.domain.Floor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class ElevatorMoveCommandHandler implements CommandHandler<ElevatorMoveCommand> {

    ElevatorRepository elevatorRepository;
    BuildingFloorRepository buildingFloorRepository;
    EventSender eventSender;

    @SneakyThrows
    @Override
    public void handle(ElevatorMoveCommand command) {

        Integer id = command.getId();

        Elevator elevator = elevatorRepository.requiredGet(id);

        elevator.clearHistory();
        elevator.move(floor -> consumeFinish(floor, elevator.getId()));

        log.info("elevator with id {} was arrived to floor {}", id, elevator.getCurrentFloor());
    }

    private void consumeFinish(Floor floor, Integer id) {
        updateBuildingFloor(floor);
        eventSender.send(new ElevatorFinishedEvent(id));
    }

    private void updateBuildingFloor(Floor floor) {
        BuildingFloor buildingFloor = buildingFloorRepository.requireGet(floor.getFloor());
        buildingFloor.removeDirection(floor.getDirection());
    }
}
