package com.fortum.codechallenge.elevators.backend.common.utils;

import java.util.function.Predicate;
import java.util.function.Supplier;

public class AssertUtils {

    public static <T> void isTrue(T input, Predicate<T> predicate, Supplier<RuntimeException> runtimeExceptionSupplier) {

        if (!predicate.test(input)) {
            throw runtimeExceptionSupplier.get();
        }
    }

    public static <T> void isFalse(T input, Predicate<T> predicate, Supplier<RuntimeException> runtimeExceptionSupplier) {

        isTrue(input, predicate.negate(), runtimeExceptionSupplier);
    }

    public static <T> void isTrue(Boolean condition, Supplier<RuntimeException> runtimeExceptionSupplier) {

        if (!condition) {
            throw runtimeExceptionSupplier.get();
        }
    }

    public static <T> void isFalse(Boolean condition, Supplier<RuntimeException> runtimeExceptionSupplier) {

        isTrue(!condition, runtimeExceptionSupplier);
    }
}
