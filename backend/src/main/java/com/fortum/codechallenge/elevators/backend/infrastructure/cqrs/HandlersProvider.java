package com.fortum.codechallenge.elevators.backend.infrastructure.cqrs;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;

interface HandlersProvider {

    CommandHandler<Object> getHandler(Object command);
}
