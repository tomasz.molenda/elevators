package com.fortum.codechallenge.elevators.backend.common;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Getter
public class ConfigurationProvider {

    int numberOfElevators;
    int minimumNumberOfFloor;
    int maximumNumberOfFloor;
    int startFloor;
    int movingTimeInMilliseconds;
    int waitingTimeInMilliseconds;

    @Autowired
    ConfigurationProvider(@Value("${com.fortum.codechallenge.numberOfElevators}") int numberOfElevators,
                          @Value("${com.fortum.codechallenge.minimumNumberOfFloor}") int minimumNumberOfFloor,
                          @Value("${com.fortum.codechallenge.maximumNumberOfFloor}") int maximumNumberOfFloor,
                          @Value("${com.fortum.codechallenge.startFloor}") int startFloor,
                          @Value("${com.fortum.codechallenge.movingTimeInMilliseconds}") int movingTimeInMilliseconds,
                          @Value("${com.fortum.codechallenge.waitingTimeInMilliseconds}") int waitingTimeInMilliseconds
                          ) {
        this.numberOfElevators = numberOfElevators;
        this.minimumNumberOfFloor = minimumNumberOfFloor;
        this.maximumNumberOfFloor = maximumNumberOfFloor;
        this.startFloor = startFloor;
        this.movingTimeInMilliseconds = movingTimeInMilliseconds;
        this.waitingTimeInMilliseconds = waitingTimeInMilliseconds;
    }
}
