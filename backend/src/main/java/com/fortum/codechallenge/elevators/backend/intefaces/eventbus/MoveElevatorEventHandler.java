package com.fortum.codechallenge.elevators.backend.intefaces.eventbus;

import com.fortum.codechallenge.elevators.backend.application.Gate;
import com.fortum.codechallenge.elevators.backend.application.command.BuildingFloorCheckCommand;
import com.fortum.codechallenge.elevators.backend.application.command.ElevatorMoveCommand;
import com.fortum.codechallenge.elevators.backend.application.command.FloorRequestElevatorCommand;
import com.fortum.codechallenge.elevators.backend.application.eventbus.ElevatorFinishedEvent;
import com.fortum.codechallenge.elevators.backend.application.eventbus.ElevatorRequestedEvent;
import com.fortum.codechallenge.elevators.backend.application.eventbus.WaitingBuildingFloorFoundedEvent;
import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class MoveElevatorEventHandler {

    Gate gate;

    @Subscribe
    @AllowConcurrentEvents
    public void handle(ElevatorRequestedEvent event) {

        Integer id = event.getId();

        ElevatorMoveCommand command = new ElevatorMoveCommand(id);
        gate.dispatch(command);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(ElevatorFinishedEvent event) {

        Integer id = event.getId();

        BuildingFloorCheckCommand command = new BuildingFloorCheckCommand();
        gate.dispatch(command);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(WaitingBuildingFloorFoundedEvent event) {

        Direction direction = event.getDirection();
        Integer floor = event.getFloor();

        gate.dispatch(new FloorRequestElevatorCommand(floor, direction));
    }

}
