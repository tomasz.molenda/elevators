package com.fortum.codechallenge.elevators.backend.infrastructure.eventbus;

import com.fortum.codechallenge.elevators.backend.application.EventSender;
import com.fortum.codechallenge.elevators.backend.common.Event;
import com.google.common.eventbus.EventBus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class EventBusSender implements EventSender {

    EventBus eventBus;

    @Override
    public void send(Event event) {

        eventBus.post(event);

        log.info("elevator id {} | Received event {}", event.getId(), event.getClass().getSimpleName());
    }
}
