package com.fortum.codechallenge.elevators.backend.application.service.nearestElevator;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.common.utils.StreamUtils;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class NearestElevatorFindService {

    List<NearestElevatorStrategy> nearestElevatorStrategies;

    public Optional<Elevator> findNearestElevator(List<Elevator> elevators, Integer floorNumber, Direction direction) {

        List<Elevator> sortedElevators = listSortedElevatorByDistanceDesc(elevators, floorNumber);

        for (Elevator elevator: sortedElevators) {

            Optional<NearestElevatorStrategy> strategy = StreamUtils.findOneElement(nearestElevatorStrategies, nearestElevatorStrategy ->
                    nearestElevatorStrategy.isApplicable(elevator, direction, floorNumber));

            if (strategy.isPresent()) {
                return Optional.of(strategy.get().apply(elevator));
            }
        }

        return Optional.empty();
    }

    private List<Elevator> listSortedElevatorByDistanceDesc(List<Elevator> elevators, Integer floorNumber) {
        return elevators.stream()
                    .sorted(Comparator.comparing(elevator -> Math.abs(elevator.getCurrentFloor() - floorNumber)))
                    .collect(Collectors.toUnmodifiableList());
    }
}
