package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Getter
public class FloorRequestElevatorCommand {

    Integer floor;
    Direction direction;
}
