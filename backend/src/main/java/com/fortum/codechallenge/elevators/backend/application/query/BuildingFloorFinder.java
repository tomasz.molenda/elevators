package com.fortum.codechallenge.elevators.backend.application.query;

import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;

import java.util.List;

public interface BuildingFloorFinder {

    List<BuildingFloor> listBuildingFloor();
}
