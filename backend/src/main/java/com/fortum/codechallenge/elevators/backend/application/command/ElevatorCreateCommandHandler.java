package com.fortum.codechallenge.elevators.backend.application.command;

import com.fortum.codechallenge.elevators.backend.application.CommandHandler;
import com.fortum.codechallenge.elevators.backend.common.ConfigurationProvider;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.ElevatorRepository;
import com.fortum.codechallenge.elevators.backend.domain.HandlingTime;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class ElevatorCreateCommandHandler implements CommandHandler<ElevatorCreateCommand> {

    ElevatorRepository elevatorRepository;
    ConfigurationProvider configurationProvider;

    @Override
    public void handle(ElevatorCreateCommand command) {

        Integer id = command.getId();
        Integer currentFloor = command.getCurrentFloor();

        HandlingTime handlingTime = new HandlingTime(configurationProvider.getMovingTimeInMilliseconds(), configurationProvider.getWaitingTimeInMilliseconds());
        Elevator elevator = new Elevator(id, currentFloor, handlingTime);
        elevatorRepository.save(elevator);
    }
}
