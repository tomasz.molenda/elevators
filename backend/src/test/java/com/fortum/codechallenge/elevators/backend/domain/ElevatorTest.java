package com.fortum.codechallenge.elevators.backend.domain;

import com.fortum.codechallenge.elevators.backend.common.Direction;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static com.fortum.codechallenge.elevators.backend.common.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ElevatorTest {

    private final static HandlingTime HANDLING_TIME = new HandlingTime(100, 100);

    @Test
    public void shouldThrowExceptionWhenAddFloorWithDifferentDirection() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(1, UP));

        // when
        assertThatThrownBy(() -> elevator.addFloor(createFloor(2, DOWN)))

        // then
        .isInstanceOf(ElevatorException.class);
    }

    @Test
    public void shouldNotMoveToFloorWhenTheSameFloorRequests() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(0, UP));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(0);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).contains(0);
    }

    @Test
    public void shouldMoveToFloorWhenSomeFloorRequestsToUp() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(2, UP));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(2);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2);
    }

    @Test
    public void shouldMoveToFloorWhenMultipleFloorsRequestToUp() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(2, UP));
        elevator.addFloor(createFloor(3, UP));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(3);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2, 3);
    }

    @Test
    public void shouldMoveToFloorWhenSomeFloorRequestsToDown() {

        // given
        Elevator elevator = new Elevator(1, 4, HANDLING_TIME);
        elevator.addFloor(createFloor(2, DOWN));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(2);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(4, 3, 2);
    }

    @Test
    public void shouldMoveToFloorWhenMultipleFloorsRequestToDown() {

        // given
        Elevator elevator = new Elevator(1, 4, HANDLING_TIME);
        elevator.addFloor(createFloor(2, DOWN));
        elevator.addFloor(createFloor(3, DOWN));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(2);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(4, 3, 2);
    }

    @Test
    public void shouldMoveToFloorWhenArriveInternalRequest() {

        // given
        Elevator elevator = new Elevator(1, 1, HANDLING_TIME);
        elevator.addFloor(createFloor(3));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(3);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(1, 2, 3);
    }

    @Test
    public void shouldMoveToFloorWhenArriveMultipleInternalRequestsWithOrder() {

        // given
        Elevator elevator = new Elevator(1, 5, HANDLING_TIME);
        elevator.addFloor(createFloor(-2));
        elevator.addFloor(createFloor(2));
        elevator.addFloor(createFloor(6));
        elevator.addFloor(createFloor(8));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(8);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(5, 4, 3, 2, 1, 0, -1, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8);
    }

    @Test
    public void shouldMoveToFloorWhenArriveMultipleInternalRequestsAndNearestIsLower() {

        // given
        Elevator elevator = new Elevator(1, 5, HANDLING_TIME);
        elevator.addFloor(createFloor(3));
        elevator.addFloor(createFloor(2));
        elevator.addFloor(createFloor(9));
        elevator.addFloor(createFloor(8));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(9);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(5, 4, 3, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    @Test
    public void shouldMoveToFloorWhenArriveMultipleInternalRequestsAndHaveTheSameDistance() {

        // given
        Elevator elevator = new Elevator(1, 5, HANDLING_TIME);
        elevator.addFloor(createFloor(7));
        elevator.addFloor(createFloor(3));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(3);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(5, 6, 7, 6, 5, 4, 3);
    }

    @Test
    public void shouldMoveToFloorWhenFirstFloorRequestsDownAndNextInternalRequestsToTheSameFloor() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(2, DOWN));
        elevator.addFloor(createFloor(2));

        // when
        elevator.move(floor -> {});

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(2);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2);
    }

    @Test
    public void shouldThrowExceptionWhenFirstInternalRequestsUpAndNextFloorRequestsDown() {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(2));

        // when
        assertThatThrownBy(() -> elevator.addFloor(createFloor(2, DOWN)))

        // then
        .isInstanceOf(ElevatorException.class);
    }

    @Test
    public void shouldMoveToFlorWhenInternalRequestDuringMoving() throws Exception {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(5, DOWN));
        Thread t1 = new Thread(() -> elevator.move(floor -> {}));
        t1.start();

        // when
        TimeUnit.MILLISECONDS.sleep(2 * HANDLING_TIME.getMovingTime());
        Thread t2 = new Thread(() -> elevator.addFloor(createFloor(6)));
        t2.start();

        t1.join();
        t2.join();

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(6);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2, 3, 4, 5, 6);
    }

    @Test
    public void shouldMoveToFlorFirstAcceptDirectionFromFloor() throws Exception {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(5, DOWN));
        Thread t1 = new Thread(() -> elevator.move(floor -> {}));
        t1.start();
        TimeUnit.MILLISECONDS.sleep(4 * HANDLING_TIME.getMovingTime());
        Thread t2 = new Thread(() -> elevator.addFloor(createFloor(2)));
        t2.start();

        // when
        Thread t3 = new Thread(() -> elevator.addFloor(createFloor(6)));
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(6);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2, 3, 4, 5, 4, 3, 2, 3, 4, 5, 6);
    }

    @Test
    public void shouldAcceptFloorRequestWhenDirectionIsTheSame() throws Exception {

        // given
        Elevator elevator = new Elevator(1, 0, HANDLING_TIME);
        elevator.addFloor(createFloor(5));
        Thread t1 = new Thread(() -> elevator.move(floor -> {}));
        t1.start();
        TimeUnit.MILLISECONDS.sleep(2 * HANDLING_TIME.getMovingTime());

        // when
        Thread t2 = new Thread(() -> elevator.addFloor(createFloor(4, UP)));
        t2.start();

        t1.join();
        t2.join();

        //then
        assertThat(elevator.getCurrentFloor()).isEqualTo(5);
        assertThat(elevator.isBusy()).isFalse();
        assertThat(elevator.getHistory()).containsExactly(0, 1, 2, 3, 4, 5);
    }

    private Floor createFloor(Integer number, Direction direction) {
        return new Floor(number, direction);
    }

    private Floor createFloor(Integer number) {
        return new Floor(number, NONE);
    }
}
