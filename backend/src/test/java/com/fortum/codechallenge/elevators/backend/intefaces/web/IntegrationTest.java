package com.fortum.codechallenge.elevators.backend.intefaces.web;

import com.fortum.codechallenge.elevators.backend.common.ConfigurationProvider;
import com.fortum.codechallenge.elevators.backend.common.Direction;
import com.fortum.codechallenge.elevators.backend.domain.BuildingFloor;
import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.fortum.codechallenge.elevators.backend.common.Direction.UP;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.util.Assert.isTrue;

/**
 * Boiler plate test class to get up and running with a test faster.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles({"test"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class IntegrationTest {

    @Autowired
    private ElevatorControllerEndPoints elevatorController;

    @Autowired
    private FloorControllerEndPoints floorController;

    @Autowired
    private ConfigurationProvider configurationProvider;

    private int movingTime;
    private int numberOfElevators;
    private int startFloor;

    @BeforeEach
    public void setUp() {
        movingTime = configurationProvider.getMovingTimeInMilliseconds();
        numberOfElevators = configurationProvider.getNumberOfElevators();
        startFloor = configurationProvider.getStartFloor();
    }

    @Test
    public void shouldNotArriveElevator() {

        // given
        Integer floor0 = 0;

        // when
        elevatorController.requestElevator(floor0, UP);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor0));
        assertThat(listHistoryOfElevatorInFloor(floor0))
                .contains(0);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldArriveElevator() {

        // given
        Integer floor3 = 3;

        // when
        elevatorController.requestElevator(floor3, UP);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor3));
        assertThat(listHistoryOfElevatorInFloor(floor3))
                .containsExactly(0, 1, 2, 3);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldUseNextElevatorWhenOneIsSoFar() {

        // given
        Integer floor10 = 10;
        Integer floor3 = 3;

        elevatorController.requestElevator(floor10, UP);

        // when
        elevatorController.requestElevator(floor3, UP);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor10, floor3));
        assertThat(listHistoryOfElevatorInFloor(floor10))
                .containsExactly(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        assertThat(listHistoryOfElevatorInFloor(floor3))
                .containsExactly(0, 1, 2, 3);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldUseOneElevatorWhenElevatorIsAlreadyRunning() throws Exception {

        // given
        Integer floor10 = 10;
        Integer waitTime = 5 * movingTime;
        Integer floor8 = 8;

        Thread t1 = new Thread(() -> elevatorController.requestElevator(floor10, UP));
        t1.start();

        //when
        TimeUnit.MILLISECONDS.sleep(waitTime);
        Thread t2 = new Thread(() -> elevatorController.requestElevator(floor8, UP));
        t2.start();
        t1.join();
        t2.join();

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor10));
        assertThat(listHistoryOfElevatorInFloor(floor10))
                .containsExactly(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldUseOneElevatorWhenElevatorIsSoClose() {

        // given
        Integer floor4 = 4;
        Integer floor5 = 5;

        elevatorController.requestElevator(floor5, UP);
        assertThat(listHistoryOfElevatorInFloor(floor5))
                .containsExactly(0, 1, 2, 3, 4, 5);

        //when
        elevatorController.requestElevator(floor4, UP);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor4));
        assertThat(listHistoryOfElevatorInFloor(floor4))
                .containsExactly(5, 4);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldElevatorGoesToExceptedFloorWhenRequestComeFromInternal() {

        // given
        Integer floor4 = 4;

        Integer id = elevatorController.listElevators()
                .stream()
                .findFirst()
                .map(Elevator::getId)
                .orElseThrow();

        //when
        elevatorController.internalRequestElevator(id, floor4);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor4));
        assertThat(listHistoryOfElevatorInFloor(floor4))
                .containsExactly(0, 1, 2, 3, 4);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldElevatorGoesToExceptedFloorWhenRequestComeFromInternalAndElevatorIsMoving() throws Exception {

        // given
        Integer floor10 = 10;
        Integer waitTime = 5 * movingTime;
        Integer floor8 = 8;

        Thread t1 = new Thread(() -> elevatorController.requestElevator(floor10, UP));
        t1.start();

        //when
        TimeUnit.MILLISECONDS.sleep(waitTime);
        Integer id = elevatorController.listElevators().stream()
                .filter(Elevator::isBusy)
                .map(Elevator::getId)
                .findFirst()
                .orElseThrow();
        Thread t2 = new Thread(() -> elevatorController.internalRequestElevator(id, floor8));
        t2.start();
        t1.join();
        t2.join();

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor10));
        assertThat(listHistoryOfElevatorInFloor(floor10))
                .containsExactly(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldElevatorStayWhenRequestToCurrentFloor() {

        // given
        Integer floor4 = 4;

        Integer id = elevatorController.listElevators()
                .stream()
                .findFirst()
                .map(Elevator::getId)
                .orElseThrow();

        elevatorController.internalRequestElevator(id, floor4);

        //when
        elevatorController.internalRequestElevator(id, floor4);

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor4));
        assertThat(listHistoryOfElevatorInFloor(floor4))
                .containsExactly(0, 1, 2, 3, 4);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldElevatorsGoToExceptedFloorWhenRequestComeFromTheSameFloorButDifferentDirections() throws Exception {

        // given
        Integer floor5 = 5;

        //when
        Thread t1 = new Thread(() -> elevatorController.requestElevator(floor5, UP));
        Thread t2 = new Thread(() -> elevatorController.requestElevator(floor5, Direction.DOWN));
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        // then
        assertThat(elevatorController.listElevators())
                .extracting(Elevator::getCurrentFloor)
                .containsExactlyInAnyOrder(prepareElevatorsWithFloor(floor5, floor5));
        assertThat(listHistoryOfElevatorInFloor(floor5))
                .containsExactly(0, 1, 2, 3, 4, 5);
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    @Test
    public void shouldFloorBeHandlingWhenFirstElevatorIsFree() throws Exception {

        // given
        List<Thread> threads = IntStream.range(1, configurationProvider.getNumberOfElevators() + 1)
                .mapToObj(value -> new Thread(() -> elevatorController.internalRequestElevator(value, configurationProvider.getMaximumNumberOfFloor())))
                .collect(Collectors.toUnmodifiableList());

        threads.forEach(Thread::start);
        TimeUnit.MILLISECONDS.sleep(5 * configurationProvider.getMovingTimeInMilliseconds());

        // when
        for (Thread thread : threads) {
            thread.join();
        }
        elevatorController.requestElevator(2, UP);

        // then
        //noinspection unchecked
        assertThat(floorController.listBuildingFloors())
                .extracting(BuildingFloor::getWaitingDirections)
                .containsOnly(Sets.newHashSet());
    }

    private Integer[] prepareElevatorsWithFloor(Integer... movedElevators) {
        isTrue(movedElevators.length <= numberOfElevators, "movedElevators can not be greater than numberOfElevators");
        Integer[] elevatorsOnDefaultFloor = new Integer[numberOfElevators - movedElevators.length];
        Arrays.fill(elevatorsOnDefaultFloor, startFloor);
        return ArrayUtils.addAll(elevatorsOnDefaultFloor, movedElevators);
    }

    private List<Integer> listHistoryOfElevatorInFloor(Integer floor) {
        return elevatorController.listElevators()
                .stream()
                .filter(elevatorViewModel -> elevatorViewModel.getCurrentFloor().equals(floor))
                .map(Elevator::getHistory)
                .findAny()
                .orElseGet(ArrayList::new);
    }
}
