package com.fortum.codechallenge.elevators.backend.application.service.nearestElevator;

import com.fortum.codechallenge.elevators.backend.domain.Elevator;
import com.fortum.codechallenge.elevators.backend.domain.Floor;
import com.fortum.codechallenge.elevators.backend.domain.HandlingTime;
import com.google.common.collect.Lists;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.fortum.codechallenge.elevators.backend.common.Direction.DOWN;
import static com.fortum.codechallenge.elevators.backend.common.Direction.UP;

class NearestElevatorFindServiceTest {

    private final static HandlingTime HANDLING_TIME = new HandlingTime(100, 100);

    private NearestElevatorFindService nearestElevatorFindService;

    @BeforeEach
    public void setUp() {
    	nearestElevatorFindService = new NearestElevatorFindService(Lists.newArrayList(
    	        new NotBusyStrategy(),
    	        new GoingToTheSameDownDirectionStrategy(),
    	        new GoingToTheSameUpDirectionStrategy()
                )
        );
    }

    @Test
    public void shouldFindNearestElevatorWhenAllAreInOneFloor() {

    	// given
        Elevator elevator1 = new Elevator(1, 0, HANDLING_TIME);
        Elevator elevator2 = new Elevator(2, 0, HANDLING_TIME);
        Elevator elevator3 = new Elevator(3, 0, HANDLING_TIME);
        List<Elevator> elevators = Lists.newArrayList(elevator1, elevator2, elevator3);

    	// when
        Optional<Elevator> nearestElevator = nearestElevatorFindService.findNearestElevator(elevators, 2, UP);

        // then
        Assertions.assertThat(nearestElevator).isNotEmpty().map(Elevator::getCurrentFloor).contains(0);
    }

    @Test
    public void shouldFindNearestElevatorWhenNearestIsMovingInTheSameDirection() throws Exception {

        // given
        Elevator elevator1 = new Elevator(1, 5, HANDLING_TIME);
        elevator1.addFloor(new Floor(8, UP));
        Thread t1 = new Thread(() -> elevator1.move(floor -> {}));
        t1.start();
        TimeUnit.MILLISECONDS.sleep(100);
        Elevator elevator2 = new Elevator(2, 0, HANDLING_TIME);
        Elevator elevator3 = new Elevator(3, 0, HANDLING_TIME);
        List<Elevator> elevators = Lists.newArrayList(elevator1, elevator2, elevator3);

        // when
        Optional<Elevator> nearestElevator = nearestElevatorFindService.findNearestElevator(elevators, 7, UP);
        t1.join();

        // then
        Assertions.assertThat(nearestElevator).isNotEmpty().map(Elevator::getCurrentFloor).contains(8);
        Assertions.assertThat(nearestElevator).isNotEmpty().map(Elevator::getId).contains(1);
    }

    @Test
    public void shouldFindNearestElevatorWhenNearestIsMovingInADifferentDirection() throws Exception {

        // given
        Elevator elevator1 = new Elevator(1, 5, HANDLING_TIME);
        elevator1.addFloor(new Floor(4, DOWN));
        Thread t1 = new Thread(() -> elevator1.move(floor -> {}));
        t1.start();
        Elevator elevator2 = new Elevator(2, 0, HANDLING_TIME);
        Elevator elevator3 = new Elevator(3, 0, HANDLING_TIME);
        List<Elevator> elevators = Lists.newArrayList(elevator1, elevator2, elevator3);;

        // when
        Optional<Elevator> nearestElevator = nearestElevatorFindService.findNearestElevator(elevators, 4, UP);
        t1.join();

        // then
        Assertions.assertThat(nearestElevator).isNotEmpty().map(Elevator::getCurrentFloor).contains(0);
    }

}
