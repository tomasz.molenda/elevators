import Repository from '../Repository';

const resource = 'elevators';

export default {
  getElevators() {
    return Repository.get(`${resource}`);
  },
  moveToFloor(data) {
    const { elevatorId, toFloor } = data;

    return Repository.post(`${resource}/${elevatorId}/move/floor/${toFloor}`);
  },
  requestElevator(data) {
    const { direction, floorNumber } = data;

    return Repository.post(`${resource}/request/${floorNumber}/${direction}`);
  }
};
