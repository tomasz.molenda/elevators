import ConfigurationRepository from './ConfigurationRepository';
import ElevatorRepository from './ElevatorRepository';
import FloorRepository from './FloorRepository';

const repositories = {
  configuration: ConfigurationRepository,
  elevator: ElevatorRepository,
  floor: FloorRepository,
};

export default {
  get: name => repositories[name]
};
