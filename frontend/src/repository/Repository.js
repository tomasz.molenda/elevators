import axios from 'axios';

const baseDomain = process.env.REACT_APP_API;
const baseURL = `${baseDomain}/rest/v1`;

const Repository = axios.create({ baseURL });

export default Repository;
