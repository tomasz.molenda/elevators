import Repository from '../Repository';

const resource = 'floors';

export default {
  getFloors() {
    return Repository.get(`${resource}`);
  }
};
