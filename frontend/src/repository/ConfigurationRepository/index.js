import Repository from '../Repository';

const resource = 'configuration';

export default {
  fetchConfiguration() {
    return Repository.get(`${resource}`);
  }
};
