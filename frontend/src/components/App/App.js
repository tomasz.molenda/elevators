import React from 'react';
import { Building } from '../../views';

import { ElevatorProvider } from '../../context/ElevatorContext';

import './App.css';

function App() {
  return (
    <div className="App">
      <ElevatorProvider>
        <Building />
      </ElevatorProvider>
    </div>
  );
}

export default App;
