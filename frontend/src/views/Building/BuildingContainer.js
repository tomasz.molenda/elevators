import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import useInterval from '../../hooks/useInterval';
import { fetchElevatorsList } from '../../store/actions/elevator';
import { fetchFloorsList } from '../../store/actions/floor';

import Building from './Building';

const BuildingContainer = () => {
  const dispatch = useDispatch();
  const { elevators } = useSelector(state => state.elevator);
  const { floors } = useSelector(state => state.floor);

  useInterval(() => dispatch(fetchElevatorsList()), 1000);
  useInterval(() => dispatch(fetchFloorsList()), 1000);

  const createLine = elevator => {
    return floors.map(floor => {
      const _elevator = floor.number === elevator.currentFloor ? elevator : undefined;
      return {
        elevator: _elevator
      };
    });
  };

  const lines = (() => {
    return elevators.map(elevator => createLine(elevator));
  })();

  return <Building lines={lines} />;
};

export default BuildingContainer;
