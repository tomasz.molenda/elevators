import React from 'react';
import PropTypes from 'prop-types';

import { floorNumbersPropType } from '../../../../prop-types';

import { FloorButtons } from '../FloorButtons';

import './FloorNumbers.scss';

const FloorNumbers = ({ floors }) => (
  <div className="floor_numbers">
    {floors.map(floor => {
      const { number } = floor;
      return (
        <div key={number} className="floor">
          <span>{number}</span>
          <FloorButtons floor={floor} />
        </div>
      );
    })}
  </div>
);

FloorNumbers.propTypes = {
  floors: PropTypes.arrayOf(floorNumbersPropType)
};

export default FloorNumbers;
