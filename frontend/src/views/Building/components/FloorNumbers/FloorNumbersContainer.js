import React from 'react';

import './FloorNumbers.scss';
import { useSelector } from 'react-redux';
import FloorNumbers from './FloorNumbers';

const FloorNumbersContainer = () => {
  const { floors } = useSelector(state => state.floor);
  return <FloorNumbers floors={floors} />;
};

export default FloorNumbersContainer;
