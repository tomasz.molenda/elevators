import React from 'react';
import { useDispatch } from 'react-redux';

import { requestElevator } from '../../../../store/actions/elevator';

import FloorButtons from './FloorButtons';
import { floorNumbersPropType } from '../../../../prop-types';

const FloorButtonsContainer = ({ floor }) => {
  const dispatch = useDispatch();
  const { number, waitingDirections } = floor;

  const onRequestElevator = direction => {
    const data = {
      floorNumber: number,
      direction
    };
    dispatch(requestElevator(data));
  };
  return (
    <FloorButtons
      onUpClick={() => onRequestElevator('UP')}
      onDownClick={() => onRequestElevator('DOWN')}
      upBusy={waitingDirections.includes('UP')}
      downBusy={waitingDirections.includes('DOWN')}
    />
  );
};

FloorButtonsContainer.propTypes = {
  floor: floorNumbersPropType
};
export default FloorButtonsContainer;
