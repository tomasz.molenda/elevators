import React from 'react';
import PropTypes from 'prop-types';

import './FloorButtons.scss';

const FloorButtons = ({ downBusy, onDownClick, onUpClick, upBusy }) => (
  <div className="floor_buttons">
    <div className={`half ${upBusy ? 'busy' : ''}`} onClick={onUpClick}>
      <i className="up" />
    </div>
    <div className={`half ${downBusy ? 'busy' : ''}`} onClick={onDownClick}>
      <i className="down" />
    </div>
  </div>
);

FloorButtons.propTypes = {
  downBusy: PropTypes.bool.isRequired,
  onDownClick: PropTypes.func.isRequired,
  onUpClick: PropTypes.func.isRequired,
  upBusy: PropTypes.bool.isRequired
};

export default FloorButtons;
