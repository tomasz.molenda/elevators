import React from 'react';

import './NoElevator.scss';

const NoElevator = () => <div className="no_elevator" />;

export default NoElevator;
