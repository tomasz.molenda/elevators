import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import useElevatorContext from '../../../../hooks/useElevatorContext';
import { moveToFloor } from '../../../../store/actions/elevator';

import './ElevatorButtons.scss';
import ElevatorButtons from './ElevatorButtons';

const ElevatorButtonsContainer = () => {
  const dispatch = useDispatch();
  const { floors } = useSelector(state => state.floor);
  const { elevatorId, setElevatorId, setVisible } = useElevatorContext();
  const close = () => {
    setElevatorId(undefined);
    setVisible(false);
  };

  const floorNumberClick = floorNumber => {
    const data = {
      elevatorId,
      toFloor: floorNumber
    };
    dispatch(moveToFloor(data)).then(close());
  };
  return (
    <ElevatorButtons
      elevatorId={elevatorId}
      floors={floors}
      onClick={floorNumberClick}
      onClose={close}
    />
  );
};

export default ElevatorButtonsContainer;
