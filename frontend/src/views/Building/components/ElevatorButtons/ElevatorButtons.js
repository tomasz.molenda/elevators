import React from 'react';
import PropTypes from 'prop-types';

import { floorNumbersPropType } from '../../../../prop-types';

import { Button } from '../Button';
import './ElevatorButtons.scss';

const ElevatorButtons = ({ elevatorId, floors, onClose, onClick }) => (
  <div className="elevator_buttons">
    <p>{elevatorId}</p>
    <Button text="X" onClick={onClose} />
    {floors.map(floor => {
      const { number } = floor;
      return (
        <Button key={number} text={number} onClick={() => onClick(number)} />
      );
    })}
  </div>
);

ElevatorButtons.propTypes = {
  elevatorId: PropTypes.number.isRequired,
  floors: PropTypes.arrayOf(floorNumbersPropType),
  onClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default ElevatorButtons;
