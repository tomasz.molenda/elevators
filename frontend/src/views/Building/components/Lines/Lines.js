import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { elevatorPropType } from '../../../../prop-types';

import { Line } from '../Line';

import './Lines.scss';

const Lines = ({ lines }) => {
  return (
    <Fragment>
      {lines.map((line, index) => (
        <Line key={index} line={line} />
      ))}
    </Fragment>
  );
};

Lines.propTypes = {
  lines: PropTypes.arrayOf(PropTypes.arrayOf(elevatorPropType))
};

export default Lines;
