import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = ({ onClick, text }) => (
  <div className="button" onClick={onClick}>
    {text}
  </div>
);

Button.defaultProps = {
  onClick: () => null,
  text: ''
};

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default Button;
