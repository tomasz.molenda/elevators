import React from 'react';
import { elevatorPropType } from '../../../../prop-types';

import useElevatorContext from '../../../../hooks/useElevatorContext';

import Elevator from './Elevator';

const ElevatorContainer = ({ elevator }) => {
  const { setElevatorId, setVisible } = useElevatorContext();
  const { id, isBusy } = elevator;

  const onClick = () => {
    setElevatorId(id);
    setVisible(true);
  };

  return <Elevator onClick={onClick} text={id} isBusy={isBusy} />;
};

Elevator.propTypes = {
  elevator: elevatorPropType
};

export default ElevatorContainer;
