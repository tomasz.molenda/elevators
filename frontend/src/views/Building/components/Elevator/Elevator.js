import React from 'react';
import PropTypes from 'prop-types';

import './Elevator.scss';

const Elevator = ({ onClick, isBusy, text }) => (
  <div className={`elevator ${isBusy ? "busy" : ""}`} onClick={onClick}>
    {text}
  </div>
);

Elevator.propTypes = {
  onClick: PropTypes.func.isRequired,
  isBusy: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
};

export default Elevator;
