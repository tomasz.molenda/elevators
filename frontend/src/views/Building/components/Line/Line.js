import React from 'react';
import PropTypes from 'prop-types';

import { elevatorPropType } from '../../../../prop-types';

import { Elevator } from '../Elevator';
import { NoElevator } from '../NoElevator';

import './Line.scss';

const Line = ({ line }) => {
  return (
    <div className="line">
      {line.map((floor, index) => {
        const { elevator } = floor;
        if (elevator) {
          return <Elevator key={index} elevator={floor.elevator} />;
        }
        return <NoElevator key={index} />;
      })}
    </div>
  );
};

Line.propTypes = {
  line: PropTypes.arrayOf(elevatorPropType)
};

export default Line;
