import React from 'react';
import PropTypes from 'prop-types';

import { elevatorPropType } from '../../prop-types';

import useElevatorContext from '../../hooks/useElevatorContext';

import { FloorNumbers } from './components/FloorNumbers';
import { Lines } from './components/Lines';
import { ElevatorButtons } from './components/ElevatorButtons';

import './Building.scss';

const Building = ({ lines }) => {
  const { visible } = useElevatorContext();
  return (
    <div className="building">
      <FloorNumbers />
      <Lines lines={lines} />
      {visible && <ElevatorButtons />}
    </div>
  );
};

Building.propTypes = {
  lines: PropTypes.arrayOf(PropTypes.arrayOf(elevatorPropType))
};

export default Building;
