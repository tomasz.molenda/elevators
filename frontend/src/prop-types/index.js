import PropTypes from 'prop-types';

export const elevatorPropType = PropTypes.shape({
  id: PropTypes.number,
  currentFloor: PropTypes.number,
  isBusy: PropTypes.bool,
  waiting: PropTypes.bool
});

export const floorNumbersPropType = PropTypes.shape({
  number: PropTypes.number,
  waitingDirections: PropTypes.arrayOf(PropTypes.oneOf(['', 'UP', 'DOWN']))
});
