import * as types from '../../types/floor';

import RepositoryFactory from '../.././../repository/RepositoryFactory';

const FloorRepository = RepositoryFactory.get('floor');

export const floorsListFetchRequested = () => ({
  type: types.FLOORS_LIST_FETCH_REQUESTED
});

export const floorsListFetchSuccess = ({ data }) => ({
  data,
  type: types.FLOORS_LIST_FETCH_SUCCESS
});

export const floorsListFetchError = ({ message, status }) => ({
  message,
  status,
  type: types.FLOORS_LIST_FETCH_ERROR
});

export function fetchFloorsList() {
  return dispatch => {
    dispatch(floorsListFetchRequested());
    FloorRepository.getFloors()
        .then(_data => dispatch(floorsListFetchSuccess(_data)))
        .catch(error => dispatch(floorsListFetchError(error)));
  };
}
