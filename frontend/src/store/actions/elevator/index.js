import * as types from '../../types/elevator';

import RepositoryFactory from '../.././../repository/RepositoryFactory';

const ElevatorRepository = RepositoryFactory.get('elevator');

export const elevatorsListFetchRequested = () => ({
  type: types.ELEVATORS_LIST_FETCH_REQUESTED
});

export const elevatorsListFetchSuccess = ({ data }) => ({
  data,
  type: types.ELEVATORS_LIST_FETCH_SUCCESS
});

export const elevatorsListFetchError = ({ message, status }) => ({
  message,
  status,
  type: types.ELEVATORS_LIST_FETCH_ERROR
});

export function fetchElevatorsList() {
  return dispatch => {
    dispatch(elevatorsListFetchRequested());
    ElevatorRepository.getElevators()
      .then(_data => dispatch(elevatorsListFetchSuccess(_data)))
      .catch(error => dispatch(elevatorsListFetchError(error)));
  };
}

export const elevatorMoveToFloorRequested = () => ({
  type: types.ELEVATORS_MOVE_TO_FLOOR_REQUESTED
});

export const elevatorMoveToFloorSuccess = ({ data }) => ({
  data,
  type: types.ELEVATORS_MOVE_TO_FLOOR_SUCCESS
});

export const elevatorMoveToFloorError = ({ message, status }) => ({
  message,
  status,
  type: types.ELEVATORS_MOVE_TO_FLOOR_ERROR
});

export function moveToFloor(data) {
  return async dispatch => {
    dispatch(elevatorMoveToFloorRequested());
    ElevatorRepository.moveToFloor(data)
        .then(_data => dispatch(elevatorMoveToFloorSuccess(_data)))
        .catch(error => dispatch(elevatorMoveToFloorError(error)));
  };
}

export const elevatorRequestRequested = () => ({
  type: types.ELEVATORS_REQUEST_REQUESTED
});

export const elevatorRequestSuccess = ({ data }) => ({
  data,
  type: types.ELEVATORS_REQUEST_SUCCESS
});

export const elevatorRequestError = ({ message, status }) => ({
  message,
  status,
  type: types.ELEVATORS_REQUEST_ERROR
});

export function requestElevator(data) {
  return dispatch => {
    dispatch(elevatorRequestRequested());
    ElevatorRepository.requestElevator(data)
        .then(_data => dispatch(elevatorRequestSuccess(_data)))
        .catch(error => dispatch(elevatorRequestError(error)));
  };
}

