import * as types from '../../types/elevator';

const INITIAL_STATE = {};

const elevatorReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ELEVATORS_LIST_FETCH_SUCCESS:
      return (() => {
        if (!action.data) {
          return;
        }

        const elevators = action.data.map(_obj => {
          return {
            id: _obj.id,
            currentFloor: _obj.currentFloor,
            isBusy: _obj.busy,
            waiting: _obj.waiting
          };
        });
        return {
          ...state,
          elevators
        };
      })();
    default:
      return state;
  }
};

export default elevatorReducer;
