import { combineReducers } from 'redux';

import elevator from './elevator';
import floor from './floor';

const reducers = combineReducers({
  elevator,
  floor
});

export default reducers;
