import * as types from '../../types/floor';

const INITIAL_STATE = {};

const elevatorReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.FLOORS_LIST_FETCH_SUCCESS:
      return (() => {
        if (!action.data) {
          return;
        }

        const floors = action.data.map(_obj => {
          return {
            number: _obj.floor,
            waitingDirections: [..._obj.waitingDirections]
          };
        });
        return {
          ...state,
          floors
        };
      })();
    default:
      return state;
  }
};

export default elevatorReducer;
