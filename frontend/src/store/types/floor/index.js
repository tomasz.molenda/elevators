export const FLOORS_LIST_FETCH_REQUESTED = 'FLOORS_LIST_FETCH_REQUESTED';
export const FLOORS_LIST_FETCH_SUCCESS = 'FLOORS_LIST_FETCH_SUCCESS';
export const FLOORS_LIST_FETCH_ERROR = 'FLOORS_LIST_FETCH_ERROR';
