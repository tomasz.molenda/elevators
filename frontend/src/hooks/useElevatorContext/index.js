import { useContext } from 'react';
import ElevatorContext from '../../context/ElevatorContext';

const useElevatorContext = () => useContext(ElevatorContext);

export default useElevatorContext;
