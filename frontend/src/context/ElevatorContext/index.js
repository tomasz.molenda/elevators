import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

const ElevatorContext = createContext(null);

export const ElevatorProvider = ({ children }) => {
  const [visible, setVisible] = useState(false);
  const [elevatorId, setElevatorId] = useState(undefined);

  return (
    <ElevatorContext.Provider
      value={{
        elevatorId,
        setElevatorId,
        setVisible,
        visible
      }}
    >
      {children}
    </ElevatorContext.Provider>
  );
};

ElevatorProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export default ElevatorContext;
